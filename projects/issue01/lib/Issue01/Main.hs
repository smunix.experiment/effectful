module Issue01.Main where

import Data.Functor (($>))
import Data.IORef
import Effectful
import Effectful.Dispatch.Dynamic (interpret, reinterpret, send)
import Effectful.State.Static.Local (evalState, get)

type Ref = IORef Int

data RefOp :: Effect where
  Wr :: Int -> RefOp m ()
  Rd :: RefOp m Int

type instance DispatchOf RefOp = Dynamic

wr :: (RefOp :> es) => Int -> Eff es ()
wr i = send $ Wr i

rd :: (RefOp :> es) => Eff es Int
rd = send Rd

runRef1 :: Eff [RefOp, IOE] a -> IO a
runRef1 =
  runEff . interpret \_ cmd -> do
    ref <- liftIO do newIORef @Int 0
    case cmd of
      Wr x -> liftIO do writeIORef ref x
      Rd -> liftIO do readIORef ref

runRef2 :: Eff [RefOp, IOE] a -> IO a
runRef2 act = do
  st <- newIORef @Int 0
  runEff $
    interpret
      ( \_ cmd -> do
          case cmd of
            Wr x -> liftIO do writeIORef st x
            Rd -> liftIO do readIORef st
      )
      act

sz :: Int
sz = 1

namedTest :: String -> IO a -> IO a
namedTest n act = do
  print $ n <> " started"
  a <- act
  print $ n <> " ended"
  return a

program :: (RefOp :> es) => Eff es Int
program = do
  wr 1
  wr 2
  rd

test1 :: IO ()
test1 = namedTest "runRef1" do runRef1 program >>= print

test2 :: IO ()
test2 = namedTest "runRef2" do runRef2 program >>= print

test3 :: IO ()
test3 = namedTest "IO a" do
  ref <- newIORef 0
  writeIORef ref 1
  writeIORef ref 2
  readIORef ref >>= print

main :: IO ()
main = do
  test1
  test2
  test3
