{
  description = "Calvin Å Hobbes : Type qualifiers, Theory Å Practice";

  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nix-filter.url = "github:numtide/nix-filter";
  inputs.effectful = {
    url = "github:haskell-effectful/effectful?ref=master";
    flake = false;
  };

  outputs = { self, flake-utils, nix-filter, devshell, nixpkgs, ... }@inputs:
    with nixpkgs.lib;
    with flake-utils.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        substr = replaceStrings [ "." ] [ "" ];
        ghcs = [ "default" "902" "923" ];
        version = ghc:
          "${ghc}.${substring 0 8 self.lastModifiedDate}.${
            self.shortRev or "dirty"
          }";
        config = { };
        overlays.devshell = devshell.overlay;
        overlays.default = f: p:
          let
            defaultGhc =
              (replaceStrings [ "." ] [ "" ] p.haskellPackages.ghc.version);

            ghcVersion = "ghc${defaultGhc}";

            mkHaskellPackages = hspkgs:
              (hspkgs.override (old: {
                overrides = composeExtensions (old.overrides or (_: _: { }))
                  (_: hp:
                    {
                      # mkDerivation = args:
                      #   hp.mkDerivation (args // {
                      #     enableLibraryProfiling = false;
                      #     doCheck = false;
                      #     doHaddock = false;
                      #   });
                    });
              })).extend (hf: hp:
                with f.haskell.lib;
                composeExtensions (hf: hp:
                  let
                    packages = attrsets.mapAttrs' (n: _: {
                      name = "${n}";
                      value = dontHaddock (disableLibraryProfiling
                        ((hf.callCabal2nix "${n}" (with nix-filter.lib;
                          filter {
                            root = "${self}/projects/${n}";
                            exclude = [ (matchExt "cabal") ];
                          }) { }).overrideAttrs (old: { })));
                    }) (attrsets.filterAttrs (_: v: v == "directory")
                      (builtins.readDir "${self}/projects"));
                  in packages) (hf: hp: {
                    effectful-core = disableLibraryProfiling (dontHaddock
                      (dontCheck (doJailbreak ((hf.callCabal2nix "effectful"
                        (with nix-filter.lib;
                          filter {
                            root = "${inputs.effectful}/effectful-core";
                            exclude = [ ];
                          }) { }).overrideAttrs (old: {
                            version =
                              "${substr hp.ghc.version}-${substr old.version}";
                          })))));
                    effectful = disableLibraryProfiling (dontHaddock
                      (dontCheck (doJailbreak ((hf.callCabal2nix "effectful"
                        (with nix-filter.lib;
                          filter {
                            root = "${inputs.effectful}/effectful";
                            exclude = [ ];
                          }) { }).overrideAttrs (old: {
                            version =
                              "${substr hp.ghc.version}-${substr old.version}";
                          })))));
                  }) hf hp);

            # all haskellPackages
            allHaskellPackages = let
              cases = listToAttrs (map (n: {
                name = "${n}";
                value = mkHaskellPackages
                  f.haskell.packages."${if n == "default" then
                    "${ghcVersion}"
                  else
                    "ghc${n}"}";
              }) ghcs);
            in cases;

            # all packages
            allPackages = ps:
              let
                result = recurseIntoAttrs (lists.fold (e: r: e // r) { } (map
                  (ghc: {
                    "${ghc}" = recurseIntoAttrs (lists.fold (e: r: e // r) { }
                      (map (p: { "${p}" = allHaskellPackages."${ghc}"."${p}"; })
                        ps));
                  }) ghcs));
              in result // { "default" = result.default."${lists.head ps}"; };

            # make dev shell
            mkDevShell = g: pkg:
              p.devshell.mkShell {
                name = "${pkg}-${g}";
                packages = with f;
                  with f.allHaskellPackages."${g}"; [
                    f.ghcid
                    (ghcWithPackages (p:
                      with p; [
                        "${pkg}"
                        cabal-install
                        p.effectful
                        p.effectful-core
                        ghc
                        haskell-language-server
                      ]))
                  ];
              };

            # all packages
            allDevShells = ps:
              let
                result = recurseIntoAttrs (lists.fold (e: r: e // r) { } (map
                  (ghc: {
                    "${ghc}" = recurseIntoAttrs (lists.fold (e: r: e // r) { }
                      (map (p: { "${p}" = mkDevShell ghc p; }) ps));
                  }) ghcs));
              in result // { "default" = result.default."${lists.head ps}"; };
          in {
            haskellPackages = allHaskellPackages.default;
            inherit allHaskellPackages allDevShells allPackages;
          };

        pkgs = import nixpkgs {
          inherit system config;
          overlays = [ overlays.devshell overlays.default ];
        };

      in with pkgs.lib;
      let
        projects = attrsets.mapAttrsToList (n: _: "${n}")
          (attrsets.filterAttrs (_: v: v == "directory")
            (builtins.readDir "${self}/projects"));
      in rec {
        inherit overlays;
        packages =
          flattenTree (pkgs.recurseIntoAttrs (pkgs.allPackages projects));
        devShells =
          flattenTree (pkgs.recurseIntoAttrs (pkgs.allDevShells projects));
      });
}
